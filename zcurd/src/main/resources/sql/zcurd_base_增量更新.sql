/* 逻辑删除	addBy：钟世云 2018.07.16 */
ALTER TABLE `zcurd_head` ADD `delete_flag_field` VARCHAR(50)  NULL  DEFAULT NULL  COMMENT '删除标志字段'  AFTER `handle_class`;

/* 在线表单	addBy：钟世云 2020.03.29 */
ALTER TABLE `zcurd_head` ADD `include_func` VARCHAR(100) NOT  NULL  DEFAULT '*'  COMMENT '包含功能' AFTER `delete_flag_field`;
